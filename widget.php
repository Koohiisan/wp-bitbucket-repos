<?php
/*
Plugin Name: WP BitBucket Repos
Plugin URI: https://bitbucket.org/Koohiisan/wp-bitbucket-repos
Description: A WordPress plugin to list public BitBucket repositories, with pertinent details returned on repo activity.
Version: 1.0
Author: Luke Gerhardt
Author URI: http://www.lukegerhardt.com/
License: GPL2

    Copyright 2016  Luke Gerhardt

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License,
    version 2, as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301  USA
*/

require_once "widget-repos.php";
add_action("widgets_init",
    function () { register_widget("WidgetBitbucketRepos"); });