<?php
class WidgetBitbucketRepos extends WP_Widget
{
    public function __construct() {
        parent::__construct("bitbucket_repos_widget", "Bitbucket Repos",
            array("description" => "A WordPress plugin to list public Bitbucket repositories, with pertinent details returned on repo activity."));
    }
    public function form($instance) {
        $title = "";
        $user = "";
        $link = "off";
        if (!empty($instance)) {
            $title = $instance["title"];
            $user = $instance["username"];
            $link = $instance["link"];
        }
        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");
        echo '<label for="'.$titleId.'"><br/>Title</label><br/>';
        echo '<input id="'.$titleId.'" type="text" name="'.$titleName.'" value="'.$title.'"/><br/>';
        $userId = $this->get_field_id("username");
        $userName = $this->get_field_name("username");
        echo '<label for="'.$userId.'"><br/>Username</label><br/>';
        echo '<input id="'.$userId.'" type="text" name="'.$userName.'" value="'.$user.'"/><br/>';
        $linkId = $this->get_field_id("link");
        $linkName = $this->get_field_name("link");
        echo '<br/><input id="'.$linkId.'" type="checkbox" name="'.$linkName.'" value="on" '.checked($link,'on',false).'/>';
        echo '<label for="'.$linkId.'">Link to repositories?</label><br/><br/>';
    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);
        $values["username"] = htmlentities($newInstance["username"]);
        $values["link"] = htmlentities($newInstance["link"]);
        return $values;
    }
    public function widget($args, $instance) {
        $title = $instance["title"];
        $user = $instance["username"];
        $link = $instance["link"];
        echo '<div class="widget bottom widget_links bitbucket_repos_widget">';
        echo '<h2 class="title small bottom-3">'.$title.'<span class="line"></span></h2>';

        if($user!='') {
            $data = wp_remote_retrieve_body(wp_remote_get('https://api.bitbucket.org/2.0/repositories/'.trim($user)));
            if($data!='' && !strpos($data,'User not found')) {
                try {
                    $array = json_decode($data);
                } catch(Exception $error) {
                    $array = null;
                }
                if($array!=null) {
                    $repos = $array->values;
                    echo '<ul>';
                    foreach ($repos as $repo) {
                        echo '<li>';
                        if($link==='on') {
                            echo '<p><a href="'.$repo->links->html->href.'" target="_blank">'.$repo->name.'</a></p>';
                        } else {
                            echo '<p>'.$repo->name.'</p>';
                        }
                        echo '<p>Updated '.$this->get_timeago(strtotime($repo->updated_on)).'</p>';
                        echo '</li>';
                    }
                    echo '</ul>';
                }
            } else {
                echo '<i>[no public repositories found]</i>';
            }
        }
        echo '</div>';
    }
    function get_timeago($time) {
        $estimate_time = time() - $time;
        if( $estimate_time < 1 ) {
            return 'less than 1 second ago';
        }
        $condition = array(
            31104000 => 'year',
            2592000 => 'month',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
        );
        foreach($condition as $secs => $str) {
            $d = $estimate_time / $secs;
            if($d >= 1) {
                $r = round($d);
                return 'about '.$r.' '.$str.($r > 1 ? 's' : '' ).' ago';
            }
        }
    }
}